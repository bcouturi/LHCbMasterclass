from ROOT import TFile
from ROOT import gSystem
gSystem.Load("libLbMclass")
from ROOT import LbMclassTrack
from ROOT import TObjArray
from ROOT import TPointsArray3D
from ROOT import TObjString, TTree



file = TFile("mydata.root", "recreate")
track1 = LbMclassTrack()
track1.particleId = TObjString("D0 ")
track1.px = 1
track1.py = 2
track1.pz = 3
track1.mass = 42

points = TPointsArray3D()
points.SetPoint(0, 0, 0, 0)
points.SetPoint(1, 1, 1, 1)
points.SetPoint(2, 2, 2, 2)
track1.track = points;

track1.printTrack()

array = TObjArray()
array.Add(track1)

tree = TTree("MCEvent", "Masterclass Event list")
tree.Branch("event", array)
#  tree->SetBranchAddress("event", &array);
tree.Fill()
tree.Write()