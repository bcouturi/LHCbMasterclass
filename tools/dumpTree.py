#!/usr/bin/env python
import os
import sys
from ROOT import TFile
from ROOT import TTree
from ROOT import gSystem
from ROOT import TObjArray

# Looking up the path for the  Masterclass event shared object
file_path = os.path.abspath(__file__)
dir_path = os.path.dirname(file_path)
(parent_path, thisdir) = os.path.split(dir_path)
libname = os.path.join(parent_path, "libLbMclass")
gSystem.Load(libname)
from ROOT import LbMclassTrack


file = TFile(sys.argv[1])
tree = file.Get("MCEvent")
array = TObjArray()
tree.SetBranchAddress("event", array)
nb = tree.GetEntries()
for i in range(nb):
    print "======================> Event: %s" % i
    tree.GetEntry(i)
    for t in array: 
        t.printTrack()


