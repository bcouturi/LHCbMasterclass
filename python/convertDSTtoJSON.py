#!/usr/bin/env python

#
# Script to extract events from stripped DST to LHCb Masterclass format
#
#

import PartProp.Service
import os
import json
import sys

from Gaudi.Configuration import *
from Configurables import DaVinci
from AnalysisPython import Dir, Functors
from GaudiPython.HistoUtils import book
from GaudiPython.Bindings import gbl
from DaVinciTools import Tools as DVTools
from ROOT import Double
LHCb = gbl.LHCb
Track = LHCb.Track
XYZPoint= gbl.ROOT.Math.XYZPoint

location = "/Event/CharmCompleteEvent/Phys/D2HHLTUnbiasedD02HHLine/Particles"
dvlocation = "/Event/CharmCompleteEvent/Phys/D2HHLTUnbiasedD02HHLine/decayVertices"
pvLocation = "Rec/Vertex/Primary"

xmin=-5
ymin=-5
zmin=-50
xmax=5
ymax=5

HCAL_Z = 13500
MUON_Z = 15000
zmax=MUON_Z

class EventCache:
  """ Util class to keep some event state """
  
  def __init__(self):
    self.tracksdone = []

    
class GaudiManager:
  """ Wrap Gaudi calls """
  
  def __init__(self):
    self.startGaudi()

  #
  # GAUDI Setup & iterator
  #
  ##############################################################################
  def startGaudi(self): 
    dv = DaVinci()
    dv.DDDBtag = 'dddb-20120831'
    dv.CondDBtag = 'cond-20120831'
    dv.DataType = '2012'
    dv.EvtMax = 10000
    dv.HistogramFile = 'histos.root'
    dv.TupleFile = 'tuples.root'
    dv.HistogramFile = ""
    dv.TupleFile = ""
    self.dv = dv
    
    # Phase 2: GaudiPython
    from GaudiPython.Bindings import gbl, AppMgr, Helper
    appMgr = AppMgr(outputlevel=4)
    appMgr.initialize()
    self.appMgr = appMgr
    
    import atexit
    atexit.register(appMgr.exit)

    # some short-cuts to services
    self.evtSvc = appMgr.evtSvc()
    self.toolSvc = appMgr.toolsvc()
    self.histSvc = appMgr.histSvc()
    self.evtSel = appMgr.evtSel()
    self.ppSvc = appMgr.ppSvc()
    self.tExp = self.toolSvc.create("TrackMasterExtrapolator", interface="ITrackExtrapolator")
    self.relPVFinder = self.toolSvc.create(DVTools.P2PVWithIPChi2, interface = 'IRelatedPVFinder')
    self.distanceCalculator = self.toolSvc.create('LoKi::DistanceCalculator' , interface='IDistanceCalculator') 
    self.nextEvent = Functors.NextEvent(appMgr, appMgr.EvtMax)
    self.evtCount=0

  def stopGaudi(self):
    self.appMgr.stop()
    self.appMgr.finalize()


  def next(self):
    return self.nextEvent()


  #
  # Tools
  #
  ##############################################################################
  def propagateTrack(self, t, zstep, zstart,  zfinal):
    """ Propagate the track in the detector and return a TPointsArray3D"""
    #print "Propagate track called with zfinal:", zfinal
    trackjson = []
    s = t.firstState().clone()
    p = s.position()
    z = zstart
    sgn = (zfinal - zstep) /(abs(zfinal - zstep))
    idx = 0
    #print "--- writeTrack --- zstart: %s zfinal:%s zstep: %s " % (zstart, zfinal, zstep)
    #while (sgn * z < sgn * zfinal and p.x() < xmax and p.y() < ymax):
    while (sgn * z < sgn * zfinal):
      self.tExp.propagate(t, z, s)
      p = s.position()
      trackjson.append((int(p.x()), int(p.y()), int(p.z())))
      z = z + self.getStep(zstep, z)
      idx = idx + 1
    return  trackjson


  def getImpactParameter(self, particle, vertex):
    """ Wraps up the IDistanceCalculator """
    ip=Double(0)
    ipchi2=Double(0)
    res = self.distanceCalculator.distance(particle, vertex, ip, ipchi2)
    if res.isFailure(): 
      return None
    else: 
      return (ip, ipchi2)


  def getTrackFirstMeasurementZ(self, track):

    if track == None:
      return None

    z = None
    for s in track.states():
      if s.LocationToString(s.location()) == "FirstMeasurement":
        z = s.z()
        break
    return z

  def getStep(self, defaultVal, z):
    """ Adapt the step according to the position in the detector """
    retval = 200
    if z < 100:
      retval = 5
    elif z < 1000:
      retval = 20
    elif z < 10000:
      retval = 100
      
    return retval
    
  def writeLine(self, x0, y0, z0, x1, y1, z1):
    """ Create a TPointsArray3D with the given coordinates """
    trackjson = []
    trackjson.append(( int(x0), int(y0), int(z0)))
    trackjson.append(( int(x1), int(y1), int(z1)))
    return trackjson

  def findBestVertex(self, particle):
    primaryVertices =  self.evtSvc[pvLocation] 
    table = self.relPVFinder.relatedPVs(particle, primaryVertices)
    relPVs = table.relations(particle)
    bestVertex = relPVs.back().to()
    return bestVertex

  #
  # Data extraction
  #
  ##############################################################################
  def processParticle(self, event, p, lvl, r0):
    """ Dump a particle and its daughters to the file """

    print "########### Processing particle: "
    print p
    
    particles = []
    allparticlesjson = []
    # First checking if this particle was already processed
    if p.proto() != None and p.proto().track() != None:
      if  p.proto().track() in event.tracksdone:
        print "Particle already processed:", p
        return (particles, allparticlesjson)

    # Stop all partciles at HCAL, except muons
    zfinal = HCAL_Z
    if p.particleID().pid() == 13:
      zfinal = MUON_Z

    p1 = None
    if p.endVertex() != None:
      p1 = p.endVertex().position()
      zfinal = p1.z()

    # Extracting particle PID
    pid = p.particleID()
    pinfo = self.ppSvc.find(pid)
    strname = pinfo.name()

    particlejson = {}
    particlejson['name'] = strname
    particlejson['m'] = p.measuredMass()
    particlejson['E'] =  p.momentum().E()
    particlejson['px'] = p.momentum().px()
    particlejson['py'] = p.momentum().py()
    particlejson['pz'] = p.momentum().pz()
    particlejson['q'] = p.charge()

    if p.proto() != None and p.proto().track() != None:
      particlejson['zFirstMeasurement'] = self.getTrackFirstMeasurementZ(p.proto().track())
    
    # Calculating the IP and IPChi2 to the best vertex
    vertex = self.findBestVertex(p)
    particlejson['pv_x'] = vertex.position().x()
    particlejson['pv_y'] = vertex.position().y()
    particlejson['pv_z'] = vertex.position().z()
    
    res = self.getImpactParameter(p, vertex)
    if res != None:
      particlejson['ipchi2'] = res[1]
      particlejson['ip'] = res[0]
    
    # Preparing the track for export
    myjsontrack = None
    if p.proto() != None:
      particlejson['trackchi2'] = p.proto().track().chi2PerDoF()
      tjson = self.propagateTrack(p.proto().track(), 5, r0[2], zfinal)
      event.tracksdone.append(p.proto().track())
      myjsontrack = tjson
    else:
      if p1 != None:
        tjson  = self.writeLine(r0[0], r0[1], r0[2], p1.x(), p1.y(), p1.z())
        myjsontrack = tjson
      else:
        raise Exception("Cannot calculate track for this particle")

    # Checking the track and adding it to the particle
    if myjsontrack == None:
      print "Adding Particle Error: Null track - Ignoring"
    elif  len(myjsontrack) == 0:
      print "Adding particle Error: Track with no points - Ignoring"
    else:
      # Now add the the object to the array
      particlejson["track"] = myjsontrack
      allparticlesjson.append(particlejson)
      print "Added particle"

    
    # Now iterate on the particle daughters
    for td in p.daughters():
      # Careful we need to use the .target() link not .data()!!!
      print "Daughter class", td.__class__.__name__
      if td.__class__.__name__ != "LHCb::Particle":
        dd = td.target()
      else:
        dd = td
      if dd != None:
        subpartsjson = self.processParticle(event, dd, lvl + 1, (p1.x(), p1.y(), p1.z()))
        allparticlesjson += subpartsjson
        
    return allparticlesjson


  #
  # Main extraction method 
  #
  ##############################################################################
  def extractData(self, event):

    evtSvc = self.evtSvc
    primVertices = evtSvc['/Event/CharmCompleteEvent/Rec/Vertex/Primary']
    ptracks = []
    json = []
    
    # Process particles find the stripped DST
    particles = evtSvc[location]
    p0 = particles[0]
    pos0 = self.findBestVertex(p0).position()
    tmpjson = self.processParticle(event, p0, 0, (pos0.x(), pos0.y(), pos0.z()))
    json += tmpjson
      
    # Looping over kaons and muons
    RichDLLmu = 101
    RichDLLpi = 102
    RichDLLk  = 103
    ProbNNe = 700  # The ANN probability for the electron hypothesis
    ProbNNmu = 701 # The ANN probability for the muon hypothesis
    ProbNNpi = 702 # The ANN probability for the pion hypothesis
    ProbNNk = 703  # The ANN probability for the kaon hypothesis
    ProbNNp = 704  # The ANN probability for the proton hypothesis
    ProbNNghost = 705# The ANN probability for the ghost hypothesis
    
    pions = evtSvc['/Event/Phys/StdLoosePions/Particles']
    for p in pions:
      print "ProbNNpi:", p.proto().info(ProbNNpi, -9999)
      if  p.proto().info(ProbNNpi, -9999) < 0.5:
        print "Ignoring pion with ProbNNpi: %s" %  p.proto().info(ProbNNpi, -9999), p
        continue
      pos = self.findBestVertex(p).position()
      tmpjson =  self.processParticle(event,  p, 0, (pos.x(), pos.y(), -100))
      json += tmpjson
      
    kaons = evtSvc['/Event/Phys/StdLooseKaons/Particles']
    for p in kaons:
      if  p.proto().info(ProbNNk, -9999) < 0.5:
        print "Ignoring kaon with ProbNNk: %s" %  p.proto().info(ProbNNk, -9999), p
        continue

      pos = self.findBestVertex(p).position()
      tmpjson = self.processParticle(event, p, 0, (pos.x(), pos.y(), -100))
      json += tmpjson

    muons = evtSvc['/Event/Phys/StdLooseMuons/Particles']
    for p in muons:
      if  p.proto().info(ProbNNmu, -9999) < 0.5:
        print "Ignoring Muon with ProbNNmu: %s" %  p.proto().info(ProbNNmu, -9999), p
        continue

      pos = self.findBestVertex(p).position()
      tmpjson = self.processParticle(event, p, 0, (pos.x(), pos.y(), -100))
      json += tmpjson

    return json

if __name__ == '__main__':

  if len(sys.argv) == 1:
    print "Please specify name of DST file"
    exit(1)

  filename = sys.argv[1]
  from GaudiConf import IOHelper
  IOHelper('ROOT').inputFiles([
    filename
    ], clear=True)
  FileCatalog().Catalogs += [ 'xmlcatalog_file:charmcomplete.xml' ]
  
  
  # Now starting gaudi
  gmgr = GaudiManager()
  index = 0
  while gmgr.next():

    index += 1
    print "#  Event: %d" % index
    print "############################################################"

    # First some setup
    event = EventCache()
    jsondata = gmgr.extractData(event)

    with open("event_%d.json" % index, 'w') as outfile:
      json.dump(jsondata, outfile)
    
  gmgr.stopGaudi()

