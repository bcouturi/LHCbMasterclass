# LHCb Masterclass exercise

For more information about this tool please see:

http://lhcb-public.web.cern.ch/lhcb-public/en/LHCb-outreach/masterclasses/en/


## Requirements

This tool requires ROOT version 5.X (http://root.cern.ch) to be installed, with the OpenGL and libtable features
(configured with *--enable-table* *--enable-opengl*).

Configuration is done using CMake (http://www.cmake.org/)

## Building

Choosing n as the number of available data files:

    cd LHCbMasterclass
    cmake -DNB_DATA_FILES=n .
    make 


